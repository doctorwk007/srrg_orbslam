#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <opencv2/core/core.hpp>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

#include "tf/transform_broadcaster.h"

using namespace std;

std::string tf_frame, tf_child_frame;
Eigen::Matrix4f Camera_T(Eigen::Matrix4f::Identity());

class ImageGrabber {
public:
  ImageGrabber(ORB_SLAM2::System* pSLAM):mpSLAM(pSLAM){}  
  void GrabStereo(const sensor_msgs::ImageConstPtr& msgLeft,const sensor_msgs::ImageConstPtr& msgRight);
  ORB_SLAM2::System* mpSLAM;
  bool do_rectify;
  cv::Mat M1l,M2l,M1r,M2r;
};

const char* banner [] = {
  "stereo_orbslam",
  "",
  "usage: stereo_node -v <vocabulary> -s <settings> -tl <topic-left> -tr <topic-right>",
  "-tl       <string>        topic name of left image, default: '/camera_left/rgb/image_raw'",
  "-tr       <string>        topic name of right image, default: '/camera_right/rgb/image_raw'",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-tf       <string>        tf_frame to publish, default: '/camera'",
  "-tf-child <string>        tf_child_frame to publish, default: '/base_link'",
  "-ng       <flag>          no gui",
  "-h        <flag>          this help",
  0
};

int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  // params
  std::string left_topic_name = "/camera_left/rgb/image_raw";
  std::string right_topic_name = "/camera_right/rgb/image_raw";
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  tf_frame = "/camera";
  tf_child_frame = "/base_link";
  bool use_gui = true;

  int c = 1;  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-tl")) {
      c++;
      left_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-tr")) {
      c++;
      right_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-tf")) {
      c++;
      tf_frame = argv[c];
    } else if (!strcmp(argv[c], "-tf-child")) {
      c++;
      tf_child_frame = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 

  std::cerr << "left_topic     (-tl):         " << left_topic_name << std::endl;
  std::cerr << "right_topic    (-tr):         " << right_topic_name << std::endl;
  std::cerr << "vocabulary     (-v):          " << vocabulary_filename << std::endl;
  std::cerr << "settings       (-s):          " << settings_filename << std::endl;
  std::cerr << "tf_frame       (-tf):         " << tf_frame << std::endl;
  std::cerr << "tf_child_frame (-tf-child):   " << tf_child_frame << std::endl;
  std::cerr << "use_gui        (-ng):         " << use_gui << std::endl;

  if(vocabulary_filename.empty() || settings_filename.empty()) {
    return 1;
  }
  
  ros::init(argc, argv, "ORBSLAM2_Stereo");
  ros::start();

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::STEREO,use_gui);

  ImageGrabber igb(&SLAM);

  stringstream ss(argv[3]);
	ss >> boolalpha >> igb.do_rectify;

  if(igb.do_rectify)
    {      
      // Load settings related to stereo calibration
      cv::FileStorage fsSettings(argv[2], cv::FileStorage::READ);
      if(!fsSettings.isOpened())
        {
          cerr << "ERROR: Wrong path to settings" << endl;
          return -1;
        }

      cv::Mat K_l, K_r, P_l, P_r, R_l, R_r, D_l, D_r;
      fsSettings["LEFT.K"] >> K_l;
      fsSettings["RIGHT.K"] >> K_r;

      fsSettings["LEFT.P"] >> P_l;
      fsSettings["RIGHT.P"] >> P_r;

      fsSettings["LEFT.R"] >> R_l;
      fsSettings["RIGHT.R"] >> R_r;

      fsSettings["LEFT.D"] >> D_l;
      fsSettings["RIGHT.D"] >> D_r;

      int rows_l = fsSettings["LEFT.height"];
      int cols_l = fsSettings["LEFT.width"];
      int rows_r = fsSettings["RIGHT.height"];
      int cols_r = fsSettings["RIGHT.width"];

      if(K_l.empty() || K_r.empty() || P_l.empty() || P_r.empty() || R_l.empty() || R_r.empty() || D_l.empty() || D_r.empty() ||
         rows_l==0 || rows_r==0 || cols_l==0 || cols_r==0)
        {
          cerr << "ERROR: Calibration parameters to rectify stereo are missing!" << endl;
          return -1;
        }

      cv::initUndistortRectifyMap(K_l,D_l,R_l,P_l.rowRange(0,3).colRange(0,3),cv::Size(cols_l,rows_l),CV_32F,igb.M1l,igb.M2l);
      cv::initUndistortRectifyMap(K_r,D_r,R_r,P_r.rowRange(0,3).colRange(0,3),cv::Size(cols_r,rows_r),CV_32F,igb.M1r,igb.M2r);
    }

  ros::NodeHandle nh;

  message_filters::Subscriber<sensor_msgs::Image> left_sub(nh, left_topic_name, 1);
  message_filters::Subscriber<sensor_msgs::Image> right_sub(nh, right_topic_name, 1);
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
  message_filters::Synchronizer<sync_pol> sync(sync_pol(10), left_sub,right_sub);
  sync.registerCallback(boost::bind(&ImageGrabber::GrabStereo,&igb,_1,_2));

  ros::spin();

  // Stop all threads
  SLAM.Shutdown();

  // Save camera trajectory
  SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory_TUM_Format.txt");
  SLAM.SaveTrajectoryTUM("FrameTrajectory_TUM_Format.txt");
  SLAM.SaveTrajectoryKITTI("FrameTrajectory_KITTI_Format.txt");

  ros::shutdown();

  return 0;
}

void ImageGrabber::GrabStereo(const sensor_msgs::ImageConstPtr& msgLeft,const sensor_msgs::ImageConstPtr& msgRight)
{
  // Copy the ros image message to cv::Mat.
  cv_bridge::CvImageConstPtr cv_ptrLeft;
  try
    {
      cv_ptrLeft = cv_bridge::toCvShare(msgLeft);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  cv_bridge::CvImageConstPtr cv_ptrRight;
  try
    {
      cv_ptrRight = cv_bridge::toCvShare(msgRight);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  cv::Mat curr_T;
  
  if(do_rectify)
    {
      cv::Mat imLeft, imRight;
      cv::remap(cv_ptrLeft->image,imLeft,M1l,M2l,cv::INTER_LINEAR);
      cv::remap(cv_ptrRight->image,imRight,M1r,M2r,cv::INTER_LINEAR);
      curr_T = mpSLAM->TrackStereo(imLeft,imRight,cv_ptrLeft->header.stamp.toSec());
    }
  else
    {
      curr_T = mpSLAM->TrackStereo(cv_ptrLeft->image,cv_ptrRight->image,cv_ptrLeft->header.stamp.toSec());
    }

  if(!curr_T.rows || !curr_T.cols)
    return;
  Eigen::Matrix4f Tr, Tr_inverse;
  for(size_t i=0; i<4; ++i)
    for(size_t j=0; j<4; ++j)
      Tr(i,j) = curr_T.at<float>(i,j);

  Tr_inverse = Tr.inverse();

  Eigen::Matrix3f rot = Tr_inverse.block<3,3>(0,0);
    
  static tf::TransformBroadcaster tf_broadcaster;
  tf::Transform tf_content;
  tf::Vector3 tf_translation(Tr_inverse(0,3), Tr_inverse(1,3), Tr_inverse(2,3));
  Eigen::Quaternionf qi(rot);
  tf::Quaternion tf_quaternion(qi.x(), qi.y(), qi.z(), qi.w());
  tf_content.setOrigin(tf_translation);
  tf_content.setRotation(tf_quaternion);

  tf::StampedTransform tf_msg(tf_content, cv_ptrLeft->header.stamp, tf_frame, tf_child_frame);
  tf_broadcaster.sendTransform(tf_msg);

}


