#include <unistd.h>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <opencv2/core/core.hpp>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

using namespace std;

void LoadImages(const string &strFile,
                vector<string> &vstrImageFilenames,
                vector<double> &vTimestamps);

const char* banner [] = {
  "monocular_orbslam",
  "",
  "usage: monocular_app -v <vocabulary> -s <settings> -data <path/to/dataset>",
  "-data     <string>        path to dataset containing rgb.txt file",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-ng       <flag>          no gui",
  "-h        <flag>          this help",
  0
};


int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }
  int c = 1;

  //params
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  std::string dataset_filename = "";
  bool use_gui = true;

  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-data")) {
      c++;
      dataset_filename = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 

  std::cerr<<"dataset_filename (-data):       "<< dataset_filename << std::endl;
  std::cerr<<"vocabulary       (-v):          "<< vocabulary_filename << std::endl;
  std::cerr<<"settings         (-s):          "<< settings_filename << std::endl;
  std::cerr<<"use_gui          (-ng):         "<< use_gui << std::endl;  
  
  if(vocabulary_filename.empty() ||
     settings_filename.empty() ||
     dataset_filename.empty()) {
    return 1;
  }
  
  // Retrieve paths to images
  vector<string> vstrImageFilenames;
  vector<double> vTimestamps;
  LoadImages(dataset_filename+"/rgb.txt", vstrImageFilenames, vTimestamps);

  int nImages = vstrImageFilenames.size();

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::MONOCULAR,use_gui);
    
  // Vector for tracking time statistics
  vector<float> vTimesTrack;
  vTimesTrack.resize(nImages);

  cout << endl << "-------" << endl;
  cout << "Start processing sequence ..." << endl;
  cout << "Images in the sequence: " << nImages << endl << endl;

  // Main loop
  cv::Mat im;
  for(int ni=0; ni<nImages; ni++)
    {
      // Read image from file
      im = cv::imread(dataset_filename+"/"+vstrImageFilenames[ni],CV_LOAD_IMAGE_UNCHANGED);
      double tframe = vTimestamps[ni];

      if(im.empty())
        {
          cerr << endl << "Failed to load image at: "
               << dataset_filename << "/" << vstrImageFilenames[ni] << endl;
          return 1;
        }

      std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
      // Pass the image to the SLAM system
      SLAM.TrackMonocular(im,tframe);
      std::chrono::steady_clock::time_point t2 = std::chrono::steady_clock::now();

      double ttrack= std::chrono::duration_cast<std::chrono::duration<double> >(t2 - t1).count();

      vTimesTrack[ni]=ttrack;

      // Wait to load the next frame
      double T=0;
      if(ni<nImages-1)
        T = vTimestamps[ni+1]-tframe;
      else if(ni>0)
        T = tframe-vTimestamps[ni-1];

      if(ttrack<T)
        usleep((T-ttrack)*1e6);
    }

  // Stop all threads
  SLAM.Shutdown();

  // Tracking time statistics
  sort(vTimesTrack.begin(),vTimesTrack.end());
  float totaltime = 0;
  for(int ni=0; ni<nImages; ni++)
    {
      totaltime+=vTimesTrack[ni];
    }
  cout << "-------" << endl << endl;
  cout << "median tracking time: " << vTimesTrack[nImages/2] << endl;
  cout << "mean tracking time: " << totaltime/nImages << endl;

  // Save camera trajectory
  SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

  return 0;
}

void LoadImages(const string &strFile, vector<string> &vstrImageFilenames, vector<double> &vTimestamps)
{
  ifstream f;
  f.open(strFile.c_str());

  // skip first three lines
  string s0;
  getline(f,s0);
  getline(f,s0);
  getline(f,s0);

  while(!f.eof())
    {
      string s;
      getline(f,s);
      if(!s.empty())
        {
          stringstream ss;
          ss << s;
          double t;
          string sRGB;
          ss >> t;
          vTimestamps.push_back(t);
          ss >> sRGB;
          vstrImageFilenames.push_back(sRGB);
        }
    }
}
