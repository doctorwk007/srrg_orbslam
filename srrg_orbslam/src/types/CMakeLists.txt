add_library(srrg_orbslam_types_library SHARED
  MapPoint.cc
  Map.cc
  Frame.cc
  KeyFrame.cc
  KeyFrameDatabase.cc
)

target_link_libraries(srrg_orbslam_types_library
  srrg_orbslam_detector_library
  srrg_orbslam_correspondence_finder_library
  srrg_orbslam_utils_library
  ${OpenCV_LIBS}
  ${EIGEN3_LIBS}
  ${DBoW2_LIBRARIES}
)
